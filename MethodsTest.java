public class MethodsTest{
	public static void main (String[] args){
		
		SecondClass sc = new SecondClass();
		
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(12, 4.4);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		System.out.println(sumSquareRoot(3, 6));
		String s1 = "Hello";
		String s2 = "Goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		int b = sc.addTwo(50);
		System.out.println(b);
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int a){
		System.out.println("Inside the method one input no return " + a);
		
	}
	public static void methodTwoInputNoReturn (int x, double y){
		System.out.println(x);
		System.out.println(y);
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int x, int y){
	double z = x+y;
	z = Math.sqrt(z);
	return z;
	}		
		

}